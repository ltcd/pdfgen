export class ApiError {
    constructor(
        public developerMessage: string,
        public userMessage: string = null, // No user messages for now
        public code: string = null, // No codes for now
    ) {}
}

export class ApiResponse {
    constructor(
        public error: ApiError | null,
        public data: any = null,
    ) {}
}