import { MissingTemplateVariablesError } from './types/MissingTemplateVariablesError';
import { NonExistingTemplateError } from './types/NonExistingTemplateError';
import { Injectable } from '@nestjs/common';
import { readFileSync } from 'fs';
import { join } from 'path';
import { Logger } from '@nestjs/common';
import Handlebars from 'handlebars';

@Injectable()
export class AppService {
  private readonly logger = new Logger(AppService.name);

  public getPdf(fileName: string, variables: object): string {
    let template: string;

    try {
      template = readFileSync(join(__dirname, './../templates', `${fileName}.hbs`), 'utf8');
    } catch {
      throw new NonExistingTemplateError(fileName);
    }

    const missingVariables = AppService.getMissingVariables(template, Object.keys(variables));

    if (missingVariables.length) {
      throw new MissingTemplateVariablesError(missingVariables.join(', '));
    }

    return Handlebars.compile(template) (variables);
  }

  private static getMissingVariables(template: string, variables: string[]): string[] {
    return (template.match(/{{[{]?(.*?)[}]?}}/g) || [])
      .map(str => str.replace(/({{\s*|\s*}})/g, ''))
      .filter(match => !variables.includes(match));
  }
}
