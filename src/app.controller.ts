import { MissingTemplateVariablesError } from './types/MissingTemplateVariablesError';
import { Get, Controller, Res, Header, Query, Param, HttpStatus, Logger } from '@nestjs/common';
import { AppService } from './app.service';
import { create, CreateOptions } from 'html-pdf';
import { NonExistingTemplateError } from './types/NonExistingTemplateError';
import { Response } from 'express';
import { ApiResponse, ApiError } from './types/ApiResponse';

// Might be part of the request later. Decided to use as default for now.
const options: CreateOptions = { format: 'Letter' };

const withoutPdfExtenstion = (fileName: string) =>
  fileName.replace(/.pdf$/, '');

@Controller()
export class AppController {
  private readonly logger = new Logger(AppService.name);

  constructor(private readonly appService: AppService) {}

  @Get(':fileName')
  root(@Res() res: Response, @Param('fileName') fileName, @Query() query) {
    let html: string;

    try {
      html = this.appService.getPdf(withoutPdfExtenstion(fileName), query);
    } catch (error) {
      this.logger.error(error);

      if (error instanceof NonExistingTemplateError) {
        return res
          .status(HttpStatus.NOT_FOUND)
          .json(
            new ApiResponse(
              new ApiError(`File "${fileName}" does not exist.`),
            ),
          );
      }

      if (error instanceof MissingTemplateVariablesError) {
        return res
          .status(HttpStatus.BAD_REQUEST)
          .json(
            new ApiResponse(
              new ApiError(`Missing parameters: ${error.message}.`),
            ),
          );
      }
    }

    res.type('application/pdf');
    create(html, options).toStream((err, stream) => {
      if (err) return res.end(err.stack);
      stream.pipe(res);
    });
  }
}
