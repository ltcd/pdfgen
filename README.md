# pdfgen

## Description

Microservice for generating PDFs.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
# development
$ yarn start

# development watch mode (recommended for dev)
$ yarn start:dev

# incremental rebuild (webpack)
$ yarn webpack
$ yarn start:hmr

# production mode
$ yarn start:prod
```

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

